Base 16 Monokai Dark Theme with Light Panels for Brackets
============================

Based on [Base16 Monokai Dark](https://github.com/enricodangelo/brackets-themes/tree/master/enricodangelo.base16-monokai-dark-theme).
