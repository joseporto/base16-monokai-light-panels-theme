# base16-monokai-light-panels-theme Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.2]
### Fixed
- convertion of colors to variables
- fixed quick edit feature inline editor
